-- Table: public.worker_shift

-- DROP TABLE public.worker_shift;

CREATE TABLE public.worker_shift
(
    worker_id serial NOT NULL,
    shift_id serial NOT NULL,
    CONSTRAINT worker_shift_pkey PRIMARY KEY (worker_id, shift_id),
    CONSTRAINT fk_shift FOREIGN KEY (shift_id)
        REFERENCES public.shift (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_worker FOREIGN KEY (worker_id)
        REFERENCES public.worker (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.worker_shift
    OWNER to postgres;