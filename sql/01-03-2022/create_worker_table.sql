-- Table: public.worker

-- DROP TABLE public.worker;

CREATE TABLE public.worker
(
    id serial NOT NULL,
    first_name character varying COLLATE pg_catalog."default" NOT NULL,
    second_name character varying COLLATE pg_catalog."default" NOT NULL,
    phone_number character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT worker_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.worker
    OWNER to postgres;