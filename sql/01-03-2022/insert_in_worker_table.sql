INSERT INTO public.worker(
	id, first_name, second_name, phone_number)
	VALUES (1, 'John', 'Johnson', '+11111');

INSERT INTO public.worker(
	id, first_name, second_name, phone_number)
	VALUES (2, 'Kate', 'Katerinova', '+12345');

INSERT INTO public.worker(
	id, first_name, second_name, phone_number)
	VALUES (3, 'Dave', 'Minchen', '+67890');

INSERT INTO public.worker(
	id, first_name, second_name, phone_number)
	VALUES (4, 'Tanya', 'Ivanova', '+00000');