-- Table: public.shift_type

-- DROP TABLE public.shift_type;

CREATE TABLE public.shift_type
(
    id integer NOT NULL,
    description character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT shift_type_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.shift_type
    OWNER to postgres;