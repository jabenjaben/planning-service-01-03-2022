-- Table: public.shift

-- DROP TABLE public.shift;

CREATE TABLE public.shift
(
    id serial NOT NULL,
    date date NOT NULL,
    shift_type integer NOT NULL,
    CONSTRAINT shift_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.shift
    OWNER to postgres;