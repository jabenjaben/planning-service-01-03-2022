package com.example.demo;

import com.example.demo.entity.dto.DtoShift;
import com.example.demo.entity.dto.DtoWorker;
import com.example.demo.entity.model.Worker;
import com.example.demo.service.WorkerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PlanningApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WorkerService workerService;

//    @Before
//    public void setUp() {
//        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void getAllShiftTypeTest() {
        String body = this.restTemplate.getForObject("/shiftType/all", String.class);
        assertThat(body).isEqualTo("" +
                "[{\"id\":1,\"description\":\"NIGHT\"}," +
                "{\"id\":2,\"description\":\"MORNING\"}," +
                "{\"id\":3,\"description\":\"AFTERNOON\"}]" +
                "");
    }

    @Test
    public void addTwoShiftForOnePersonTest() {
        String body = this.restTemplate.getForObject("/shiftType/all", String.class);
        assertThat(body).isEqualTo("" +
                "[{\"id\":1,\"description\":\"NIGHT\"}," +
                "{\"id\":2,\"description\":\"MORNING\"}," +
                "{\"id\":3,\"description\":\"AFTERNOON\"}]" +
                "");
    }

    @Test
    public void retrieveDetailsForCourse() throws Exception {

        Set<DtoShift> dtoShiftSet = new HashSet<>();

        DtoWorker mockDtoWorker = createDtoWorker(
                297,
                "Badman",
                "Badmanov",
                "+777",
                dtoShiftSet
        );

        String exampleWorkerJson = "" +
                "{\n" +
                "    \"firstName\": \"Badman\",\n" +
                "    \"secondName\": \"Badmanov\",\n" +
                "    \"phoneNumber\": \"+777\",\n" +
                "    \"shifts\": []\n" +
                "}";

        String expected = "" +
                "{\n" +
                "    \"id\": 297,\n" +
                "    \"firstName\": \"Badman\",\n" +
                "    \"secondName\": \"Badmanov\",\n" +
                "    \"phoneNumber\": \"+777\",\n" +
                "    \"shifts\": []\n" +
                "}";

        Mockito.when(
                workerService.saveWorker(
                        Mockito.any(Worker.class))).thenReturn(ResponseEntity.ok(mockDtoWorker));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/worker/add")
                .accept(MediaType.APPLICATION_JSON).content(exampleWorkerJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse());

        JSONAssert.assertEquals(expected, result.getResponse()
                .getContentAsString(), false);
    }

    private DtoWorker createDtoWorker(Integer id,
                                      String firstName,
                                      String secondName,
                                      String phoneNumber,
                                      Set<DtoShift> shiftSet) {
        return DtoWorker.builder()
                .id(id)
                .firstName(firstName)
                .secondName(secondName)
                .phoneNumber(phoneNumber)
                .shifts(shiftSet)
                .build();
    }

    private DtoShift createDtoShift(Integer id,
                                    Date date,
                                    Integer shiftType,
                                    Set<DtoWorker> workerSet) {
        return DtoShift.builder()
                .id(id)
                .date(date)
                .shiftType(shiftType)
                .workers(workerSet)
                .build();
    }
}
