package com.example.demo.service;

import com.example.demo.entity.model.ShiftType;
import com.example.demo.repository.ShiftTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShiftTypeService {

    @Autowired
    private ShiftTypeRepository shiftTypeRepository;

    public ResponseEntity<List<ShiftType>> getAllShiftTypes() {
        return ResponseEntity.ok(shiftTypeRepository.findAll());
    }
}
