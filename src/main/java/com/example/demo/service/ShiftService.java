package com.example.demo.service;

import com.example.demo.entity.dto.DtoShift;
import com.example.demo.entity.dto.DtoWorker;
import com.example.demo.entity.exception.ResourceNotFoundException;
import com.example.demo.entity.exception.WorkerWillGetTiredException;
import com.example.demo.entity.model.Shift;
import com.example.demo.entity.model.Worker;
import com.example.demo.repository.ShiftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShiftService {

    @Autowired
    private ShiftRepository shiftRepository;

    public ResponseEntity<List<DtoShift>> getAllShifts() {
        List<DtoShift> dtoShiftListResult = listShiftsToListDto(shiftRepository.findAll());
        return ResponseEntity.ok(dtoShiftListResult);
    }

    public ResponseEntity<DtoShift> getShift(Integer id) {
        DtoShift dtoShiftResult = shiftToDto(shiftRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Shift not found for this id :: " + id)));
        return ResponseEntity.ok(dtoShiftResult);
    }

    public ResponseEntity<DtoShift> saveShift(Shift shift) {
        DtoShift dtoShiftResult = shiftToDto(shiftRepository.save(shift));
        return ResponseEntity.ok(dtoShiftResult);
    }

//    //todo: A worker never has two shifts on the same day
//    public ResponseEntity<Shift> updateShift(Integer id,
//                                             Shift shiftDetails) {
//        Shift shift = shiftRepository.findById(id)
//                .orElseThrow(() -> new ResourceNotFoundException("Shift not found for this id :: " + id));
//
//        shift.setDate(shiftDetails.getDate());
//        shift.setShiftType(shiftDetails.getShiftType());
//        shift.setWorkers(shiftDetails.getWorkers());
//        final Shift updatedShiftResult = shiftRepository.save(shift);
//        return ResponseEntity.ok(updatedShiftResult);
//    }

    public ResponseEntity<Map<String, Boolean>> deleteShift(Integer id) {
        Shift shift = shiftRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Shift not found for this id :: " + id));

        shiftRepository.delete(shift);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted shift = " + shift.toString(), Boolean.TRUE);
        return ResponseEntity.ok(response);
    }

    private List<DtoShift> listShiftsToListDto(List<Shift> shiftList) {
        List<DtoShift> dtoShiftList = new ArrayList<>();

        for (Shift shift : shiftList) {
            dtoShiftList.add(shiftToDto(shift));
        }

        return dtoShiftList;
    }

    private DtoShift shiftToDto(Shift shift) {
        Set<DtoWorker> dtoWorkerSet = new HashSet<>();

        for (Worker worker : shift.getWorkers()) {
            DtoWorker dtoWorker = DtoWorker.builder()
                    .id(worker.getId())
                    .firstName(worker.getFirstName())
                    .secondName(worker.getSecondName())
                    .phoneNumber(worker.getPhoneNumber())
                    .build();
            dtoWorkerSet.add(dtoWorker);
        }

        return DtoShift.builder()
                .id(shift.getId())
                .shiftType(shift.getShiftType())
                .date(shift.getDate())
                .workers(dtoWorkerSet)
                .build();
    }

//    private boolean isDayOfShiftsNotRepeat(Shift shiftForCheck) {
//
//        boolean isNotRepeat = true;
//
//        for (Worker worker : shiftForCheck.getWorkers()) {
//
//            Set<Date> dateShiftSet = new HashSet<>();
//            dateShiftSet.add(shiftForCheck.getDate());
//
//            for (Shift shift : worker.getShifts()) {
//                dateShiftSet.add(shift.getDate());
//            }
//
//            isNotRepeat = worker.getShifts().size() == dateShiftSet.size();
//            dateShiftSet.clear();
//        }
//        return isNotRepeat;
//    }
}
