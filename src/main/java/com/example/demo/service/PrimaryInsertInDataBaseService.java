package com.example.demo.service;

import com.example.demo.entity.enums.ShiftType;
import com.example.demo.entity.model.Shift;
import com.example.demo.entity.model.Worker;
import com.example.demo.repository.ShiftRepository;
import com.example.demo.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PrimaryInsertInDataBaseService {

    @Autowired
    ShiftRepository shiftRepository;

    @Autowired
    WorkerRepository workerRepository;

    public void updateDataBase() throws Exception{

        Shift shift1 = makeShift(new Date(1646082000000L), ShiftType.NIGHT.getCode());
        Shift shift2 = makeShift(new Date(1646082000000L), ShiftType.MORNING.getCode());
        Shift shift3 = makeShift(new Date(1646082000000L), ShiftType.AFTERNOON.getCode());

        Shift shift4 = makeShift(new Date(1646168400000L), ShiftType.NIGHT.getCode());
        Shift shift5 = makeShift(new Date(1646168400000L), ShiftType.MORNING.getCode());
        Shift shift6 = makeShift(new Date(1646168400000L), ShiftType.AFTERNOON.getCode());

        shiftRepository.save(shift1);
        shiftRepository.save(shift2);
        shiftRepository.save(shift3);
        shiftRepository.save(shift4);
        shiftRepository.save(shift5);
        shiftRepository.save(shift6);

        Set<Shift> shiftSet1 = new HashSet<>();
        shiftSet1.add(shift1);
        shiftSet1.add(shift4);

        Set<Shift> shiftSet2 = new HashSet<>();
        shiftSet2.add(shift2);
        shiftSet2.add(shift5);

        Set<Shift> shiftSet3 = new HashSet<>();
        shiftSet3.add(shift3);
        shiftSet3.add(shift6);

        workerRepository.save(makeWorker("Adam", "Adamov", "+11111", shiftSet1));
        workerRepository.save(makeWorker("Alla", "Allaeva", "+22222", shiftSet2));
        workerRepository.save(makeWorker("Ivanov", "Ivanov", "+33333", shiftSet3));

        List<Worker> workerList = workerRepository.findAll();
        System.out.println(workerList);
    }

    public Worker makeWorker(String firstName,
                             String secondName,
                             String phone,
                             Set<Shift> shifts) {
        return Worker.builder()
                .firstName(firstName)
                .secondName(secondName)
                .phoneNumber(phone)
                .shifts(shifts)
                .build();
    }

    public Shift makeShift(Date date,
                           Integer shiftType) {
        return Shift.builder()
                .date(date)
                .shiftType(shiftType)
                .build();
    }
}