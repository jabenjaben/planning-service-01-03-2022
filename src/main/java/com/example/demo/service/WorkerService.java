package com.example.demo.service;

import com.example.demo.entity.dto.DtoShift;
import com.example.demo.entity.dto.DtoWorker;
import com.example.demo.entity.exception.ResourceNotFoundException;
import com.example.demo.entity.exception.WorkerWillGetTiredException;
import com.example.demo.entity.model.Shift;
import com.example.demo.entity.model.Worker;
import com.example.demo.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WorkerService {

    @Autowired
    private WorkerRepository workerRepository;

    public ResponseEntity<List<DtoWorker>> getAllWorkers() {
        List<DtoWorker> dtoWorkerList = listWorkersToListDto(workerRepository.findAll());
        return ResponseEntity.ok(dtoWorkerList);
    }

    public ResponseEntity<DtoWorker> getWorker(Integer id) {
        DtoWorker dtoWorkerResult = workerToDto(workerRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Worker not found for this id :: " + id)));
        return ResponseEntity.ok(dtoWorkerResult);
    }

    public ResponseEntity<DtoWorker> saveWorker(Worker worker) {
        if (Boolean.FALSE.equals(isDayOfShiftsNotRepeat(worker))) throw new WorkerWillGetTiredException();
        DtoWorker dtoWorkerResult = workerToDto(workerRepository.save(worker));
        return ResponseEntity.ok(dtoWorkerResult);
    }

    public ResponseEntity<DtoWorker> updateWorker(Integer id,
                                               Worker workerDetails) {
        Worker worker = workerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Worker not found for this id :: " + id));

        if (Boolean.FALSE.equals(isDayOfShiftsNotRepeat(workerDetails))) throw new WorkerWillGetTiredException();

        worker.setFirstName(workerDetails.getFirstName());
        worker.setSecondName(workerDetails.getSecondName());
        worker.setPhoneNumber(workerDetails.getPhoneNumber());
        worker.setShifts(workerDetails.getShifts());
        final DtoWorker dtoWorkerResult = workerToDto(workerRepository.save(worker));
        return ResponseEntity.ok(dtoWorkerResult);
    }

    public ResponseEntity<Map<String, Boolean>> deleteWorker(Integer id) {
        Worker worker = workerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Worker not found for this id :: " + id));

        workerRepository.delete(worker);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted worker = " + worker.toString(), Boolean.TRUE);
        return ResponseEntity.ok(response);
    }

    private List<DtoWorker> listWorkersToListDto(List<Worker> workerList) {
        List<DtoWorker> dtoWorkerList = new ArrayList<>();

        for (Worker worker : workerList) {
            dtoWorkerList.add(workerToDto(worker));
        }

        return dtoWorkerList;
    }

    private DtoWorker workerToDto(Worker worker) {
        Set<DtoShift> dtoShiftSet = new HashSet<>();

        for (Shift shift : worker.getShifts()) {
            DtoShift dtoShift = DtoShift.builder()
                    .id(shift.getId())
                    .shiftType(shift.getShiftType())
                    .date(shift.getDate())
                    .build();

            dtoShiftSet.add(dtoShift);
        }

        return DtoWorker.builder()
                .id(worker.getId())
                .firstName(worker.getFirstName())
                .secondName(worker.getSecondName())
                .phoneNumber(worker.getPhoneNumber())
                .shifts(dtoShiftSet)
                .build();
    }

    private boolean isDayOfShiftsNotRepeat(Worker worker) {
        Set<Date> dateShiftSet = new HashSet<>();
        for (Shift shift : worker.getShifts()) {
            dateShiftSet.add(shift.getDate());
        }
        return worker.getShifts().size() == dateShiftSet.size();
    }
}