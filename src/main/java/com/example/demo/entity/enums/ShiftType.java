package com.example.demo.entity.enums;

import lombok.Getter;

@Getter
public enum ShiftType {

    NIGHT(1),
    MORNING(2),
    AFTERNOON(3);

    private final int code;

    ShiftType(int code) {
        this.code = code;
    }
}
