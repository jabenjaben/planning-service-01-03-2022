package com.example.demo.entity.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "shift")
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Shift {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Date date;
    private Integer shiftType;

    @ManyToMany(mappedBy = "shifts")
    private Set<Worker> workers;
}
