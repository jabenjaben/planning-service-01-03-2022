package com.example.demo.entity.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "worker")
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Worker {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String firstName;
    private String secondName;
    private String phoneNumber;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "worker_shift", joinColumns = {@JoinColumn(name = "worker_id")},
            inverseJoinColumns = {@JoinColumn(name = "shift_id")})
    private Set<Shift> shifts;
}

