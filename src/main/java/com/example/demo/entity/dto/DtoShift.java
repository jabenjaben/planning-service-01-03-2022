package com.example.demo.entity.dto;

import lombok.*;

import java.util.Date;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class DtoShift {
    private Integer id;
    private Date date;
    private Integer shiftType;

    private Set<DtoWorker> workers;
}
