package com.example.demo.entity.dto;

import lombok.*;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class DtoWorker {
    private Integer id;
    private String firstName;
    private String secondName;
    private String phoneNumber;

    private Set<DtoShift> shifts;
}
