package com.example.demo.entity.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class WorkerWillGetTiredException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public WorkerWillGetTiredException() {
        super("Worker have more then one shift in day!");
    }
}
