package com.example.demo.repository;

import com.example.demo.entity.model.Shift;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShiftRepository extends JpaRepository<Shift, Integer> {



}
