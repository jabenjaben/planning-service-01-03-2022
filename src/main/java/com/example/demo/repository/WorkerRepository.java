package com.example.demo.repository;

import com.example.demo.entity.model.Worker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkerRepository extends JpaRepository<Worker, Integer> {


}
