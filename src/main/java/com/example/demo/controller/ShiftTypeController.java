package com.example.demo.controller;

import com.example.demo.entity.model.ShiftType;
import com.example.demo.service.ShiftTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/shiftType")
public class ShiftTypeController {

    @Autowired
    ShiftTypeService shiftTypeService;

    @GetMapping("/all")
    public ResponseEntity<List<ShiftType>> getShiftTypes() {
        return shiftTypeService.getAllShiftTypes();
    }

}
