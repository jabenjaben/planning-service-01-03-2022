package com.example.demo.controller;

import com.example.demo.entity.dto.DtoShift;
import com.example.demo.entity.exception.ResourceNotFoundException;
import com.example.demo.entity.model.Shift;
import com.example.demo.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/shift")
public class ShiftController {

    @Autowired
    private ShiftService shiftService;

    @GetMapping("/all")
    public ResponseEntity<List<DtoShift>> getAllShifts() {
        return shiftService.getAllShifts();
    }

    @GetMapping("/{id}")
    public ResponseEntity<DtoShift> getShift(Integer id) {
        return shiftService.getShift(id);
    }

    @PostMapping("/add")
    public ResponseEntity<DtoShift> createShift(@Valid @RequestBody Shift shift) {
        return shiftService.saveShift(shift);
    }

//    @PutMapping("/update/{id}")
//    public ResponseEntity<Shift> updateShift(@PathVariable(value = "id") Integer id,
//                                             @Valid @RequestBody Shift shiftDetails) throws ResourceNotFoundException {
//        return shiftService.updateShift(id, shiftDetails);
//    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteShift(@PathVariable(value = "id") Integer id)
            throws ResourceNotFoundException {
        return shiftService.deleteShift(id);
    }
}
