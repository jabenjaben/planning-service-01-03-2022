package com.example.demo.controller;

import com.example.demo.entity.dto.DtoWorker;
import com.example.demo.entity.exception.ResourceNotFoundException;
import com.example.demo.entity.model.Worker;
import com.example.demo.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/worker")
public class WorkerController {

    @Autowired
    private WorkerService workerService;

    @GetMapping("/all")
    public ResponseEntity<List<DtoWorker>> getAllWorkers() {
        return workerService.getAllWorkers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<DtoWorker> getWorker(Integer id) {
        return workerService.getWorker(id);
    }

    @PostMapping("/add")
    public ResponseEntity<DtoWorker> createWorker(@Valid @RequestBody Worker worker) {
        return workerService.saveWorker(worker);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<DtoWorker> updateWorker(@PathVariable(value = "id") Integer id,
                                               @Valid @RequestBody Worker workerDetails) throws ResourceNotFoundException {
        return workerService.updateWorker(id, workerDetails);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteWorker(@PathVariable(value = "id") Integer id)
            throws ResourceNotFoundException {
        return workerService.deleteWorker(id);
    }
}
