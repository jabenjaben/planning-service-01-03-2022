package com.example.demo;

import com.example.demo.service.PrimaryInsertInDataBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;

@SpringBootApplication
public class PlanningApplication implements CommandLineRunner {

    @Autowired
    PrimaryInsertInDataBaseService primaryInsertInDataBaseService;

    public static void main(String[] args) {
        SpringApplication.run(PlanningApplication.class, args);
    }

    @Transactional
    @Override
    public void run(String... arg0) throws Exception {
        primaryInsertInDataBaseService.updateDataBase(); //if u want full DB
    }
}